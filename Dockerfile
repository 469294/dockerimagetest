FROM debian:latest
RUN apt-get -y update && apt-get -y install ansible
WORKDIR /
COPY . ./
